# kux-color
此插件为uts 专用的颜色转换和主题色生成工具。支持目前主流颜色格式相互转换，如 rgb，hex，hsv 和 hsl 的相互转换。

## API概览

| 方法名 | 参数 | 返回类型 | 说明 |
| --- | --- | --- | --- |
| rgb2hsl | `RGB` 见下面说明 | `HSL` 见下面说明 | `RGB`颜色值转`HSL`颜色值 |
| rgb2hsv | `RGB` 见下面说明 | `HSV` 见下面说明 | `RGB`颜色值转`HSV`颜色值
| rgb2hex | `RGB` 见下面说明 | `HEX` 十六进制字符串，如 `#00BC79` | `RGB`颜色值转`HEX`颜色值，会自动校准 `WCAG2.0` 无障碍标准
| hsl2rgb | `HSL` 见下面说明 | `RGB` 见下面说明 | `HSL`颜色值转`RGB`颜色值
| hsl2hex | `HSL` 见下面说明 | `HEX` 十六进制字符串，如 `#00BC79` | `HSL`颜色值转`HEX`颜色值
| hsv2rgb | `HSV` 见下面说明 | `RGB` 见下面说明 | `HSV`颜色值转`RGB`颜色值
| hsv2hex | `HSV` 见下面说明 | `HEX` 十六进制字符串，如 `#00BC79` | `HSV`颜色值转`HEX`颜色值
| hex2rgb | `HEX` 十六进制字符串，如 `#00BC79` | `RGB` 见下面说明 | `HEX`颜色值转`RGB`颜色值
| generate | `(color: string, options?: GenerateOptions)` `GenerateOptions见下面说明` | `string[]` | 获取基础色对应的调色板，可以直接获取 `0-9` 共`10`个色阶的调色板集合，也可以根据索引值单独获取调色板的某个颜色
| darkenColor `v1.0.2` | `DarkenColorOptions` 见下方说明 | `HEX` 十六进制字符串，如 `#00BC79` | 获取单色变暗后的颜色值
| darkenGradient `v1.0.2` | `DarkenGradientOptions` 见下方说明 | `linear-gradient` 或 `radial-gradient` 渐变色字符串，如 `linear-gradient(to right, rgb(255, 0, 0), hsl(120, 100%, 50%))` | 获取渐变色变暗后的颜色值
| extractColors `v1.0.2` | `linear-gradient` 或 `radial-gradient` 渐变色字符串，如 `linear-gradient(to right, rgb(255, 0, 0), hsl(120, 100%, 50%))` | `string[]` | 提取 `linear-gradient` 或 `radial-gradient` 渐变色字符串中的颜色值
| extractRGBValues `v1.0.2` | `rgb` 颜色字符串，如 `rgb(255, 0, 0)` | `number[]` | 提取RGB格式字符串的颜色值
| extractHSLValues `v1.0.2` | `hsl` 颜色字符串，如 `hsl(120, 100%, 50%)` | `number[]` | 提取HSL格式字符串的颜色值
| extractHSVValues `v1.0.2` | `hsv` 颜色字符串，如 `hsv(110, 80%, 20%)` | `number[]` | 提取HSV格式字符串的颜色值


## API 使用示例
### rgb2hsl 示例
```ts
import { rgb2hsl } from '@/uni_modules/kux-color';

const hslColor = rgb2hsl(255, 0, 0);
console.log(hslColor); // 输出：{ h: 0, s: 1, l: 0.5 }
```

### rgb2hsv 示例
```ts
import { rgb2hsv } from '@/uni_modules/kux-color';

const hsvColor = rgb2hsv(0, 188, 121);
console.log(hsvColor); // 输出：{ h: 159, s: 1.0, v: 0.74 }
```

### rgb2hex 示例
```ts
import { rgb2hex } from '@/uni_modules/kux-color';

const hexColor = rgb2hex(255, 87, 51);
console.log(hslColor); // 输出：#ff5733
```

### hsl2rgb 示例
```ts
import { hsl2rgb } from '@/uni_modules/kux-color';

const rgbColor = hsl2rgb(0, 1, 0.5);
console.log(rgbColor); // 输出：{ r: 255, g: 0, b: 0 }
```

### hsl2hex 示例
```ts
import { hsl2hex } from '@/uni_modules/kux-color';

const hexColor = hsl2hex(251, 0.5, 0.3);
console.log(hexColor); // 输出：#342673
```

### hsv2rgb 示例
```ts
import { hsl2rgb } from '@/uni_modules/kux-color';

const rgbColor = hsl2rgb(0, 1, 0.5);
console.log(rgbColor); // 输出：{ r: 255, g: 0, b: 0 }
```

### hsv2hex 示例
```ts
import { hsv2hex } from '@/uni_modules/kux-color';

const hexColor = hsv2hex(159, 1.0, 0.74);
console.log(hexColor); // 输出：#0ac785
```

### hex2rgb 示例
```ts
import { hex2rgb } from '@/uni_modules/kux-color';

const rgbColor = hex2rgb('#ff5733');
console.log(rgbColor); // 输出：{ r: 255, g: 87, b: 51 }
```

### generate 示例
```ts
import { generate } from '@/uni_modules/kux-color';
import { GenerateOptions } from '@/uni_modules/kux-color/utssdk/interface.uts';

// 该参数选项为可选项
const generateOptions: GenerateOptions = {
	dark: false, // 是否暗黑模式
	list: true, // 是否列表模式
	format: 'hex', // 颜色格式
	index: 6 // 索引
};
const colorList = generate('#00BC79', generateOptions);
console.log(colorList); // 输出：["#E8FFF3","#a6e8c8","#73daac","#44cd94","#2fd495","#00BC79","#0aab78","#00855F","#00694E","#004D3C"]
```

### generate 示例【暗黑模式下】
`GenerateOptions` 的 `dark` 设置为 `true` 即可

```ts
import { generate } from '@/uni_modules/kux-color';
import { GenerateOptions } from '@/uni_modules/kux-color/utssdk/interface.uts';

// 该参数选项为可选项
const generateOptions: GenerateOptions = {
	dark: true, // 是否暗黑模式
	list: true, // 是否列表模式
	format: 'hex', // 颜色格式
	index: 6 // 索引
};
const colorList = generate('#00BC79', generateOptions);
console.log(colorList); // 输出：["#004D3C","#00694E","#00855F","#0DB580","#0AC785","#4FCA9B","#6CD6AA","#94CFB4","#C0DECF","#E8FFF3"]
```

### darkenColor 示例
```uts
import { darkenColor } from '@/uni_modules/kux-color';
import { DarkenColorOptions } from '@/uni_modules/kux-color/utssdk/interface';

const darkenedColor = darkenColor({
	color: '#00BC79',
	amount: 0.1
} as DarkenColorOptions);
console.log(darkenedColor); // 输出：#0a9464
```
> **注意**
> 
> `1.0.2` 及以上版本支持

### darkenGradient 示例
```uts
import { darkenGradient } from '@/uni_modules/kux-color';
import { DarkenGradientOptions } from '@/uni_modules/kux-color/utssdk/interface';

const gradientString = 'linear-gradient(to right, rgb(255, 0, 0), #00FF00, hsl(120, 100%, 50%))';
const darkenedGradient = darkenGradient({
	gradientString: gradientString,
	amount: 0.2
} as DarkenGradientOptions);
console.log(darkenedGradient); // 输出：linear-gradient(to right, rgb(163, 0, 0), #0aa30a, hsl(120, 1, 0.28))
```
> **注意**
> 
> `1.0.2` 及以上版本支持

### extractColors 示例
```uts
import { extractColors } from '@/uni_modules/kux-color';

const gradientString = 'linear-gradient(to right, rgb(255, 0, 0), #00FF00, hsl(120, 100%, 50%))';
const darkenedColor = extractColors(gradientString);
console.log(darkenedColor); // 输出：["rgb(255, 0, 0)","#00FF00","hsl(120, 100%, 50%)"]
```
> **注意**
> 
> `1.0.2` 及以上版本支持

### extractRGBValues 示例
```uts
import { extractRGBValues } from '@/uni_modules/kux-color';

console.log(extractRGBValues('rgb(255, 0, 0)')); // 输出：[255, 0, 0]
```
> **注意**
> 
> `1.0.2` 及以上版本支持

### extractHSLValues 示例
```uts
import { extractHSLValues } from '@/uni_modules/kux-color';

console.log(extractHSLValues('hsl(120, 100%, 50%)')); // 输出：[120, 1, 0.5]
```
> **注意**
> 
> `1.0.2` 及以上版本支持

### extractHSVValues 示例
```uts
import { extractHSVValues } from '@/uni_modules/kux-color';

console.log(extractHSVValues('hsv(150, 80%, 50%)')); // 输出：[150, 0.8, 0.5]
```
> **注意**
> 
> `1.0.2` 及以上版本支持

### RGB  参数说明
| 参数名 | 类型 | 说明 |
| --- | --- | --- |
| r | `number` | 红色基础色
| g | `number` | 绿色基础色
| b | `number` | 蓝色基础色

### HSL 参数说明
| 参数名 | 类型 | 说明 |
| --- | --- | --- |
| h | `number` | hue色相，0-360范围之间
| s | `number` | saturation饱和度，0-1之间
| l | `number` | lightness明度，0-1之间

### HSV 参数说明
| 参数名 | 类型 | 说明 |
| --- | --- | --- |
| h | `number` | hue色相，0-360范围之间
| s | `number` | saturation饱和度，0-1之间
| v | `number` | value亮度，0-1之间

### GenerateOptions 参数说明
| 参数名 | 类型 | 说明 |
| --- | --- | --- |
| dark | `boolean` | 是否获取暗黑模式下的颜色，默认为 `false`
| list | `boolean` | 是否获取色阶集合，默认为 `true`
| index | `number` | 色阶颜色索引，支持 1-10，默认为 `6`
| format | `Formats` 见下面说明 | 颜色值格式，支持hex/rgb/hsl 三种格式

### DarkenColorOptions 参数说明
| 参数名 | 类型 | 说明 |
| --- | --- | --- |
| color | `string` | 输入的颜色字符串，只能写入 `hex` 颜色格式
| amount | `number` | 变暗程度，0-1之间的值
| minL | `number` | 最小亮度值，0-1之间的值，默认为0.1
| maxL | `number` | 最大亮度值，0-1之间的值，默认为 `l-amount`，既原始亮度值-变暗程度
| format | `Formats` | 输出颜色值的格式，支持 `hex`、`rgb`和 `hsl`

### DarkenGradientOptions 参数说明
| 参数名 | 类型 | 说明 |
| --- | --- | --- |
| gradientString | `string` | 输入的渐变色字符串
| amount | `number` | 变暗程度，0-1之间的值

## UTS 自定义类型
### Formats
```ts
export type Formats = 
| 'hex'
| 'rgb'
| 'hsl'
```

### RGB
```ts
export type RGB = {
	r: number;
	g: number;
	b: number;
};
```

### HSL
```ts
export type HSL = {
	h: number;
	s: number;
	l: number;
};
```

### HSV
```ts
export type HSV = {
	h: number;
	s: number;
	v: number;
};
```

### HEX
```ts
export type HEX = 
| '0'
| '1'
| '2'
| '3'
| '4'
| '5'
| '6'
| '7'
| '8'
| '9'
| 'A'
| 'B'
| 'C'
| 'D'
| 'E'
| 'F';
```

### GenerateOptions
```ts
export type GenerateOptions = {
	dark: boolean;
	list: boolean;
	index: number;
	format: Formats;
};
```

### DarkenColorOptions
```uts
export type DarkenColorOptions = {
	color: string;
	amount?: number;
	minL?: number;
	maxL?: number;
	format?: Formats;
};
```
> **注意**
> 
> `1.0.2` 及以上版本支持

### DarkenGradientOptions
```uts
export type DarkenGradientOptions = {
	gradientString: string;
	amount?: number;
}
```
> **注意**
> 
> `1.0.2` 及以上版本支持

## 兼容性
| Android | IOS-JS | Web
| --- | --- | ---
| ✅ | ✅| ✅

---
### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)
#### QQ群：870628986 [点击加入](https://qm.qq.com/q/lJOzzu6UEw)

___
### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：【F2图表、双滑块slider、炫酷效果tabbar、拖拽排序、日历拖拽选择、签名...】UniAppX 高质量UI库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手